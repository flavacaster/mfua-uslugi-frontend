import React from 'react';
import ReactDOM from 'react-dom';
import './sass/index.scss';
import {Provider} from "react-redux";
import {store} from './store/index';
import App from './layout/App';
import {BrowserRouter as Router} from "react-router-dom";
import L10N from "./Helpers";

L10N.load()
    .then(() => {
        ReactDOM.render(
            <React.StrictMode>
                <Provider store={store}>
                    <Router>
                        <App L10NLoaded={true}/>
                    </Router>
                </Provider>
            </React.StrictMode>,
            document.getElementById('root')
        );
    });


