import Type from './types';

export const setLocale = (locale) => ({type:Type.SET_LOCALE, payload:locale});
export const setToken = (token) => ({type:Type.SET_TOKEN, payload:token});
export const setUser = (user) => ({type:Type.SET_USER, payload:user});
export const toggleSpinner = (boolean) => ({type:Type.TOGGLE_SPINNER, payload:boolean});
export const getStatusEnums = () => ({type:Type.GET_ENUMS});
export const setModalControls = (obj) => ({type:Type.SET_MODAL_CONTROLS, payload:obj});
export const setModalData = (obj) => ({type:Type.SET_MODAL_DATA, payload: obj});

