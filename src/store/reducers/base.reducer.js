import Type from "../types";
const Cookies = require('cookies-js');

const initialState = {
    locale: 'ru-RU',
    token: Cookies.get('token'),
    user: {},
    showSpinner: false,
    statusEnums: {
        1: 'REQUESTED',
        2: 'READY',
        3: 'DECLINED'
    },
    modalData: {},
    showModal: function (){},
    modalActive: false
    
}

export function reducer(state = initialState, action) {
    switch (action.type) {
        case Type.SET_LOCALE:
            return {
                ...state,
                locale: action.payload,
            }
        case Type.SET_TOKEN:
            return {
                ...state,
                token: action.payload,
            }
        case Type.SET_USER:
            return {
                ...state,
                user: action.payload,
            }

        case Type.TOGGLE_SPINNER:
            return {
                ...state,
                showSpinner: action.payload
            }
        case Type.GET_ENUMS:
            return {
                ...state
            }
        case Type.SET_MODAL_DATA:
            return {
                ...state,
                modalData: action.payload,
            }
        case Type.SET_MODAL_CONTROLS:
            return {
                ...state,
                showModal: action.payload.showModal || state.showModal,
                modalActive: action.payload.modalActive
            }
        default:
            return state;
    }
}
