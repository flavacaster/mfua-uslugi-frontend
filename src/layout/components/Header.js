import React from "react";
import NavMenu from "./NavMenu";
import HeaderLogin from "./HeaderLogin";

export default function Header() {

    return(
        <div className="header">
            <div className="header__wrapper">
                <div className="header__logo logo">{window.L10N.get('logo')}</div>
                <div className="header__info">
                    <div className="header__nav-menu">
                        <NavMenu/>
                    </div>
                    <div className="header__log">
                        <HeaderLogin/>
                    </div>
                </div>
            </div>
        </div>
    )

}

