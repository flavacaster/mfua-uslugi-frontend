import React from "react";

export default function ValidationTooltip({error}) {

    return(
        <div className="validation-tooltip">{error}</div>
    )
}
