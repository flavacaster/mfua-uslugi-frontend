import React from "react";
import {NavLink} from "react-router-dom";

export default function NavMenu() {

    return(
        <>
            <NavLink className='nav-link' activeClassName='link-active' to='/orders'>{window.L10N.get('orderedServices')}</NavLink>
            <NavLink className='nav-link' activeClassName='link-active' to='/services'>{window.L10N.get('services.title')}</NavLink>
        </>
    )
}
