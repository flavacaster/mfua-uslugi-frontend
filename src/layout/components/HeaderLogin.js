import React from "react";
import {Button} from "react-bootstrap";
import {useDispatch} from "react-redux";
import * as Actions from '../../store/actions';

const Cookies = require('cookies-js');
export default function HeaderLogin() {
    const dispatch = useDispatch();

    const onClickLogout = () => {
        Cookies.set('token', null);
        dispatch(Actions.setToken(null));
    }

    return(
        <>
            <Button onClick={onClickLogout} variant={"secondary"} className="header__login-btn">{window.L10N.get('auth.log_out')}</Button>
        </>
    )
}
