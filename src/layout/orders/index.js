import {useCallback, useEffect, useRef} from "react";
import ReactDOM  from 'react-dom';
import {useDispatch, useSelector} from "react-redux";
import OrdersTable from "./OrdersTable";


export default function OrdersView() {
    const {token} = useSelector(state => state.reducer);
    const ordersContainer = useRef(null);
    const dispatch = useDispatch();
    const {statusEnums, locale} = useSelector(state => state.reducer);

    const renderTable = useCallback(() => {
        return (orders) => {
            ReactDOM.render(<OrdersTable locale={locale} dispatch={dispatch} orders={orders} statusEnums={statusEnums} token={token}/>,
                ordersContainer.current);
        }
    }, [locale, dispatch, statusEnums]);

    useEffect(() => {
        try {
            window.axios.get("/api/v1/catalog/orders",
                {headers: {"Authorization": `Bearer ${token}`, "Content-Type": "application/json"}})
                .then(res => {
                    if (!res.data.data) return;
                    renderTable()(res.data.data);
                });
        } catch (e) {
            console.error(e);
        }
    }, [renderTable, token]);



    return (
        <div className='orders'>
            <div className="orders__header">{window.L10N.get('orders.title')}</div>
            <div className="orders__content" ref={ordersContainer}>
            </div>
        </div>
    )
}
