import React from "react";
import {Table, Button} from "react-bootstrap";
import moment from "moment";
import 'moment/locale/ru';
import * as Actions from "../../store/actions";

export default function OrdersTable({orders = [], statusEnums, dispatch, locale, token}) {
    const setMomentLocale = () => {
        if (locale) {
            const l = locale.split('-')[0];
            moment.locale(l);
        }
    }
    const renderTableHeaders = () => {
        const fields = Object.keys(orders[0]);
        return <HeaderRow fields={fields}/>;
    }
    const renderTableContent = () => {
        setMomentLocale();
        return orders.map((o, idx) => {
            if (o.documentUrl) {
                const url = o.documentUrl;
                o.documentUrl = <Button
                    key={idx + 10}
                    onClick={handleDownloadDoc}
                    variant={'secondary'}
                    data-file-name={o.title}
                    data-url={url}
                    className="download-btn">
                    <div style={{pointerEvents: "none"}}>{window.L10N.get('orders.table.download')}</div>
                    <div className='g-icon-print' style={{pointerEvents: "none", backgroundImage: `url('${process.env.PUBLIC_URL}/assets/printer.svg')`}}>
                    </div>
                </Button>;
            }
            if (o.dateOrdered) {
                o.dateOrdered = moment(o.dateOrdered * 1000).format("DD MM YYYY HH:mm");
            }
            if (o.status && typeof o.status === 'number') {
                const statusNumber = o.status;
                o.status = window.L10N.get(`orders.table.orderStatus.${statusEnums[`${statusNumber}`]}`);
            }
            return <BodyRow key={idx} data={o}/>;
        });
    }

    const handleDownloadDoc = async (e) => {
        dispatch(Actions.toggleSpinner(true));
        try {
            const url = e.target.getAttribute('data-url');
            const extension = url.split(/\.(?=[^\.]+$)/)[1];

            const fileName = e.target.getAttribute('data-file-name');
            await window.axios.get(`${url}`, {responseType: "arraybuffer", headers:{"Authorization": `Bearer ${token}`}})
                .then(res => {
                    dispatch(Actions.toggleSpinner(false));
                    if (!res.data) return
                    const file = new Blob([res.data]);
                    const fileUrl = URL.createObjectURL(file);
                    const link = document.createElement('a');
                    link.download = `${fileName}.${extension}`;
                    link.href = fileUrl;
                    link.click();
                });
        } catch (e) {
            dispatch(Actions.toggleSpinner(false));
            console.error(e);
        }
    }

    return (
        <>
            {orders.length ? (<Table striped bordered hover>
                <thead>
                {renderTableHeaders()}
                </thead>
                <tbody>
                {renderTableContent()}
                </tbody>
            </Table>) : <div className="no-orders">{window.L10N.get('orders.noOrders')}</div>}
        </>
    )
}

function HeaderRow({fields}) {
    return (
        <tr>
            {fields.map((f) => {
                return (
                    <th key={f}>{window.L10N.get(`orders.table.${f}`)}</th>
                )
            })}
        </tr>
    );
}

function BodyRow({data}) {

    return (
        <tr data-order-id={data.id}>
            <td>{data.id}</td>
            <td>{data.title}</td>
            <td>{data.status}</td>
            <td>{data.dateOrdered}</td>
            <td>{data.documentUrl}</td>
        </tr>
    )
}
