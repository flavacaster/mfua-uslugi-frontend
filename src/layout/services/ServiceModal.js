import {useEffect, useReducer, useState} from "react";
import {Modal, Button, Form} from "react-bootstrap";
import * as Actions from '../../store/actions';
import ValidationTooltip from "../components/ValidationTooltip";

export default function ServiceModal({setModalActive, service, show, authToken, dispatch}) {
    let initialState = {
        fields: {},
        errors: {}
    };
    const [open, setOpen] = useState(show);

    const handleClose = () => setOpen(false);
    const [state, setState] = useReducer(reducer, initialState);

    useEffect(() => service.fields.forEach(field => handleAddInput(field.name, field)), [service.fields]);

    const handleAddInput = (name, value) => {
        setState({
            type: 'add',
            payload: {name: name, value: value}
        });
    }

    const handleInputUpdate = (name, event) => {
        setState({
            type: 'update',
            payload: {name: name, value: event.target.value}
        });
    }
    const handleValidationErrors = (name, value) => {
        setState({
            type: 'setErrors',
            payload: {name: name, value: value}
        })
    }
    const validateValue = (value) => {
        return value !== '' && value !== undefined && value !== null;
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        dispatch(Actions.toggleSpinner(true));
        const params = {
            fields: {}
        };
        const headers = {};
        headers['Authorization'] = `Bearer ${authToken}`;
        headers['Content-Type'] = `application/json`;

        service.fields.forEach(f => {
            params.fields[f.name] = state[f.name];
        })
        params.id = service.id;
        try {
            await window.axios.post('/api/v1/catalog/submit', {...params},
                {
                    headers: {
                        "Authorization": `Bearer ${authToken}`,
                        "Content-Type": "application/json"
                    }
                }).then(res => {
                dispatch(Actions.toggleSpinner(false));
                if (res.data.code === 200) {
                    handleClose();
                    dispatch(Actions.setModalData({type: 'success', content: [window.L10N.get('services.success')]}));
                    setModalActive(true);
                }

                Object.entries(res.data.data).forEach(([key, value]) => handleValidationErrors(key, value));
            }).catch(err => {
                const data = err.response.data;
                if (data) {
                    Object.entries(data.errors).forEach(([key, value]) => handleValidationErrors(key, value));
                }
            })
        } catch (e) {
            dispatch(Actions.toggleSpinner(false));
            console.error(e);
        }

    }


    return (
        <Modal show={open} centered autoFocus onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title style={{fontWeight: 600}}>{window.L10N.get('services.service')}: {service.title}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form onSubmit={handleSubmit}>
                    {Object.entries(state.fields).map(([key, value], idx) => {
                        return (
                            <Form.Group key={idx} controlId={key}>
                                <Form.Label itemID={key}>{value.title}</Form.Label>
                                <Form.Control onChange={e => handleInputUpdate(key, e)} aria-required={"true"} itemID={key}/>
                                {(state.errors[key] || state.errors['fields']) && !validateValue(state[key]) ? <ValidationTooltip error={state.errors[key] || state.errors['fields']}/> : null}
                            </Form.Group>
                        )
                    })}
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={handleSubmit} variant={'primary'}>{window.L10N.get('services.sendRequest')}</Button>
                <Button onClick={handleClose} variant={'outline-secondary'}>{window.L10N.get('modal.back')}</Button>
            </Modal.Footer>
        </Modal>
    )
}


const reducer = (state, action) => {
    switch (action.type) {
        case "add":
            state.fields[action.payload.name] = action.payload.value;
            return {...state}
        case "update":
            state[action.payload.name] = action.payload.value;
            return {...state};
        case "setErrors":
            state.errors[action.payload.name] = action.payload.value;
            return {...state}
        default:
            return state;
    }
};


