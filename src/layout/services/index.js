import React, {useCallback, useEffect, useRef, useState} from "react";
import {Button, Card, Col, Form, Row} from "react-bootstrap";
import * as Moment from 'moment';
import {render} from "@testing-library/react";
import ServiceModal from "./ServiceModal";
import {useDispatch, useSelector} from "react-redux";
import * as Actions from '../../store/actions';
import ResultModal from "../components/ResultModal";

export default function Services() {
    const [services, setServices] = useState([]);
    const [modalActive, setModalActive] = useState(false);
    const [searchString, setSearchString] = useState('');
    const cardsContainer = useRef(null);
    const {token, modalData} = useSelector(state => state.reducer);
    const dispatch = useDispatch();

    const showServiceModal = useCallback(() => {
        return (e, service) => {
            render(<ServiceModal setModalActive={setModalActive} service={service} show={true} authToken={token} dispatch={dispatch}/>);
        }
    }, [token, dispatch, setModalActive]);

    const render4ServicesInRow = useCallback(() =>{
        return (services) => {
            const result = [];
            let count = 0;
            services.forEach((s, i) => {
                if (i % 4 === 0) count++;
                if (result[count] === undefined) result[count] = [];
                result[count].push(s);
            });

            render(result.map((row, idx) => {
                return (
                    <Row key={idx} className="services__content-cards__row">
                        {row && row.map((el, idx) => {
                            return (
                                <Col className='services__content-cards__card-container' xs={3} key={idx}>
                                    <Card className={'services__content-cards__card'} id={el.id}>
                                        <Card.Header style={{fontWeight: 600}}>{el.title}</Card.Header>
                                        <Card.Body>{window.L10N.get('services.serviceDuration')}: {Moment.duration(el.duration * 1000).asHours()}{window.L10N.get('time.h')}</Card.Body>
                                        <Card.Footer>
                                            <Button onClick={e => showServiceModal()(e, el)} id={el.id}>{window.L10N.get('services.getService')}</Button>
                                        </Card.Footer>
                                    </Card>
                                </Col>
                            )
                        })}
                    </Row>
                )
            }), {container: cardsContainer.current})
        }
    }, [showServiceModal]);


    useEffect(() => {
        dispatch(Actions.toggleSpinner(true));
        try {
            window.axios.get('/api/v1/catalog', {headers: {"Authorization": `Bearer ${token}`}})
                .then(res => {
                    dispatch(Actions.toggleSpinner(false));
                    if (!res.data.data) return;
                    render4ServicesInRow()(res.data.data);
                    setServices(res.data.data);
                });
        } catch (e) {
            console.error(e);
        }
    }, []);


    const filterServices = (e) => {
        e.preventDefault();
        const value = searchString;
        const filteredServices = services.filter(serv => {
            return (serv.title.toLowerCase().indexOf(value.trim().toLowerCase()) !== -1) || (serv.id.toString() === value);
        });

        render4ServicesInRow()(filteredServices);
    }

    return (
        <div className="services">
            <div className="services__header">{window.L10N.get('services.title')}</div>
            <div className="services__content">
                <Row className="services__search-form-container">
                    <Form onSubmit={filterServices} className="services__search-form">
                        <Form.Control placeholder={window.L10N.get('searchForm.placeholder')} className="search-input" onChange={e => setSearchString(e.target.value)}/>
                        <Button className="services__search-form__btn" onClick={filterServices}>{window.L10N.get('services.search')}</Button>
                    </Form>
                </Row>
                <div className="services__content-cards" ref={cardsContainer}>
                </div>
            </div>
            <ResultModal content={modalData.content} type={modalData.type} show={modalActive} setShow={setModalActive}/>
        </div>
    )
}
