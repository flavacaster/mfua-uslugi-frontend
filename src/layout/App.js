import '../sass/common.scss';
import L10N from '../Helpers';
import Header from './components/Header';
import Footer from './components/Footer';
import React, {useEffect, useState} from "react";
import {Switch, Route, Redirect} from "react-router-dom";
import OrdersView from "./orders";
import Services from "./services";
import {useSelector, useDispatch} from "react-redux";
import Auth from "../auth";
import {Spinner} from "react-bootstrap";
import * as Actions from '../store/actions';
import ResultModal from './components/ResultModal';


export default function App({L10NLoaded}) {
    const {showSpinner, token, modalData} = useSelector(state => state.reducer);
    const dispatch = useDispatch();
    const [isAuthToken, setIsAuthToken] = useState(false);
    const [isOpenModal, showModal] = useState(false);
    useEffect(() => {
        setIsAuthToken(checkAuthToken(token));
    }, [token]);

    useEffect(() => {
        dispatch(Actions.setModalData({modalActive: isOpenModal, showModal: showModal}));
    }, [isOpenModal, showModal, dispatch]);

    return (
        <>
            {L10NLoaded ?
                <>
                    {isAuthToken && <Header/>}
                    <div className="main">
                        <div className="wrapper">
                            <Switch>
                                <Route exact path={'/orders'} render={() => redirectIfNoToken(isAuthToken, <OrdersView/>)}/>
                                <Route exact path={['', '/', '/services']} render={() => redirectIfNoToken(isAuthToken, <Services/>)}/>
                                <Route exact path={'/auth'} render={() => isAuthToken === true ? <Redirect to='/'/> : <Auth/>}/>
                            </Switch>
                        </div>
                    </div>
                    {isAuthToken && <Footer/>}
                </>
                : null}
            {(showSpinner !== undefined && showSpinner) || !L10NLoaded ?
                <div id="spinner-bg"><Spinner variant={"info"} style={{width: '10rem', height: '10rem'}} animation={'border'}/></div> : null}
            <ResultModal content={modalData.content} type={modalData.type} show={isOpenModal} setShow={showModal}/>
        </>
    )
}

function redirectIfNoToken(isAuthToken, component) {
    return !isAuthToken ? <Redirect to='/auth'/> : component;
}

function checkAuthToken(token) {
    return token !== undefined && token !== '' && token !== null;
}

