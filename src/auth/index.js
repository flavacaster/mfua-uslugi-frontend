import React, {useState} from "react";
import {Form, Button} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import * as Actions from '../store/actions'
import ValidationTooltip from "../layout/components/ValidationTooltip";
import ResultModal from "../layout/components/ResultModal";

const Cookies = require('cookies-js');

export default function Auth() {
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const [firstBlur, setFirstBlur] = useState(true);
    const [modalActive, setModalActive] = useState(false);
    const dispatch = useDispatch();
    const {modalData} = useSelector(state => state.reducer);


    const onSubmit = (e) => {
        if (e) e.preventDefault();
        if (!validateValue(login) && !validateValue(password)) return;

        const params = {
            email: login,
            password: password,
        }

        window.axios.post('/api/v1/auth', params)
            .then(res => {
                const {token} = res.data.data;
                dispatch(Actions.setUser({email: login, token: token}));
                dispatch(Actions.setToken(token));
                Cookies.set('token', token);
            }).catch(err => {
            const content = err.response.data;
            dispatch(Actions.setModalData({type: 'error', content: [content.message, content.errors]}));
            setModalActive(true);
        });
    }

    const validateValue = (value) => {
        return value !== '';
    }

    const onEnterKeySubmit = (e) => e.keyCode === 13 && validateValue(login) && validateValue(password) ? onSubmit(e) : null;

    const onChangeHandler = (e) => {
        switch (e.target.name) {
            case 'password':
                setPassword(e.target.value);
                break;
            case 'login':
                setLogin(e.target.value);
                break;
            default:
                return;
        }
    }

    return (
        <>
            <div className="auth">
                <Form className='auth__form'>
                    <Form.Group className='auth__form-group mb-3' controlId='formLogin'>
                        <Form.Label className='auth__form-label'>{window.L10N.get('auth.login')}</Form.Label>
                        <Form.Control isValid={validateValue(login)}
                                      name={'login'}
                                      onKeyUp={e => onEnterKeySubmit(e)}
                                      onChange={e => onChangeHandler(e)}
                                      itemID='formLogin'
                                      className='auth__form-input'
                                      type='text'
                                      value={login}
                                      onBlur={() => setFirstBlur(false)}/>
                        {!firstBlur && !validateValue(login) ? <ValidationTooltip error={window.L10N.get('auth.requiredField')}/> : null}
                    </Form.Group>
                    <Form.Group className='auth__form-group mb-3' controlId='formPassword'>
                        <Form.Label className='auth__form-label'>{window.L10N.get('auth.password')}</Form.Label>
                        <Form.Control isValid={validateValue(password)}
                                      name={'password'}
                                      onKeyUp={e => onEnterKeySubmit(e)}
                                      onChange={e => onChangeHandler(e)}
                                      itemID='formPassword'
                                      className='auth__form-input'
                                      type='password'
                                      value={password}
                                      onBlur={() => setFirstBlur(false)}/>
                        {!firstBlur && !validateValue(password) ? <ValidationTooltip error={window.L10N.get('auth.requiredField')}/> : null}
                    </Form.Group>
                    <Button onClick={e => onSubmit(e)} variant={"primary"} className={'auth__form-btn'}>{window.L10N.get('auth.log_in')}</Button>
                </Form>
            </div>
            <ResultModal content={modalData.content} type={modalData.type} show={modalActive} setShow={setModalActive}/>

        </>
    )
}
